(** [ε] is defined as the smallest positive number. *)
Definition ε := 1.
